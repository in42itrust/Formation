﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.Decorator
{
    class MoutardeAuCidreDecorator : Decorator
    {
        public MoutardeAuCidreDecorator(HotDogComponent hotdog) : base(hotdog)
        {
            this.hNom = "Moutarde ayu Cidre";
            this.hPrix = 3.5;

        }
    }
}