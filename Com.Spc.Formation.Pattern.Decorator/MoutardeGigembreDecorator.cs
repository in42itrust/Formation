﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.Decorator
{
    class MoutardeGigembreDecorator : Decorator
    {
        public MoutardeGigembreDecorator(HotDogComponent hotdog) : base(hotdog)
        {

            this.hNom = " Moutarde Gigembre";
            this.hPrix = 8.0; 
        }
    }
}