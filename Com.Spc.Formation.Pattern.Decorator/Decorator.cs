﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.Decorator
{
   abstract  class Decorator : HotDogComponent
    {

        HotDogComponent hBase = null;

        protected String hNom = " Decroateur indéfini";
        protected double hPrix = 0.0;

        protected Decorator ( HotDogComponent hotdog)
        {
            hBase = hotdog;
        }

        // on override le smethodes GetName et GetPrice pour les y ajouter
        // les elements qui vont s ajouter au HotDog
        public override string getName()
        {
            return string.Format("{0}, {1}", hBase.getName(), hNom);
        }

        public override double getPrice()
        {
            return hPrix + hBase.getPrice();
        }
    }
}