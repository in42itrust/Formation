﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Spc.Formation.Pattern.Decorator
{
    class Program
    {
        static void Main(string[] args)
        {

            HotDogBase hBase = new HotDogBase();
            PrintHotDogDetails(hBase);

            HotDogMeuh hMeuh = new HotDogMeuh();
            PrintHotDogDetails(hMeuh);

            HotDogBzz hBzz = new HotDogBzz();
            PrintHotDogDetails(hBzz);


            Console.WriteLine("avec supplément ...");

            MoutardeAuCidreDecorator hMoutardeCidre = new MoutardeAuCidreDecorator(hBzz);
            PrintHotDogDetails(hMoutardeCidre);

            MoutardeGigembreDecorator hMoutardeGig = new MoutardeGigembreDecorator(hMeuh);
            PrintHotDogDetails(hMoutardeGig);


            Console.ReadKey();
        }



        private static void PrintHotDogDetails( HotDogComponent hotdog)
        {

            Console.WriteLine();
            Console.WriteLine("{0} , prix : {1}", hotdog.getName(), hotdog.getPrice());
        }
    }
}
