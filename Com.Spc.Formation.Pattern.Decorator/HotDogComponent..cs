﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.Decorator
{
    public abstract class HotDogComponent
    {

        protected String hNom;
        protected Double hPrix;

        public virtual string getName()
        {
            return hNom;
        }

        public virtual double getPrice()
        {
            return hPrix;
        }

    }
}