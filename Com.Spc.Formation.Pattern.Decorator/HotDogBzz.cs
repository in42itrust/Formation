﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.Decorator
{
    class HotDogBzz : HotDogComponent
    {
        public HotDogBzz()
        {
            hNom = "hot dog Bzz";
            hPrix = 7.5;
        }
    }
}