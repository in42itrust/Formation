﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.singleton
{
 

        public class TestDisqueDur
        {
            public DateTime dt;

            private static TestDisqueDur _instance = null;

            private TestDisqueDur() { }

            public static TestDisqueDur Instance()
            {
                if (_instance == null)
                {
                    _instance = new TestDisqueDur();
                    Console.WriteLine("Démarrage du disque....");
                }

                return _instance;
            }

            public void afficheDetail()
            {
                Console.WriteLine("Disque dur démarre.");
            }
        }
    }
