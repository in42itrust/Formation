﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Spc.Formation.Pattern.singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciation du disque
            TestDisqueDur disque = TestDisqueDur.Instance();
            disque.afficheDetail();

            AutreDisque();
            Console.ReadKey();
        }

        public static void AutreDisque()
        {
            //On essaie d'instancier un nouveau disque
            TestDisqueDur disque = TestDisqueDur.Instance();
            disque.afficheDetail();
        }
    }
}
