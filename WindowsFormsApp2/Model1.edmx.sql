
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/19/2017 12:22:36
-- Generated from EDMX file: C:\Users\map\Documents\Visual Studio 2017\Projects\ConsoleApp1\WindowsFormsApp2\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Piraterie];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PirateSet'
CREATE TABLE [dbo].[PirateSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nvarchar(max)  NOT NULL,
    [Prenom] nvarchar(max)  NOT NULL,
    [Vivant] bit  NOT NULL,
    [equipage_Id] int  NOT NULL
);
GO

-- Creating table 'BateauSet'
CREATE TABLE [dbo].[BateauSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nvarchar(max)  NOT NULL,
    [capacite] int  NOT NULL,
    [Capitaine_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'PirateSet'
ALTER TABLE [dbo].[PirateSet]
ADD CONSTRAINT [PK_PirateSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BateauSet'
ALTER TABLE [dbo].[BateauSet]
ADD CONSTRAINT [PK_BateauSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [equipage_Id] in table 'PirateSet'
ALTER TABLE [dbo].[PirateSet]
ADD CONSTRAINT [FK_Equipage]
    FOREIGN KEY ([equipage_Id])
    REFERENCES [dbo].[BateauSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Equipage'
CREATE INDEX [IX_FK_Equipage]
ON [dbo].[PirateSet]
    ([equipage_Id]);
GO

-- Creating foreign key on [Capitaine_Id] in table 'BateauSet'
ALTER TABLE [dbo].[BateauSet]
ADD CONSTRAINT [FK_BateauPirate]
    FOREIGN KEY ([Capitaine_Id])
    REFERENCES [dbo].[PirateSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BateauPirate'
CREATE INDEX [IX_FK_BateauPirate]
ON [dbo].[BateauSet]
    ([Capitaine_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------