﻿using Com.Spc.Dc;
using Com.Spc.DC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Spc.DC
{
    class Program
    {
        private static Random random = new Random();

        static void Main(string[] args)
        {
            dungeonCrawler();
        }

        private static void dungeonCrawler()
        {
            Joueur Markus = new Joueur(150);
            int cptFacile = 0;
            int cptDifficile = 0;
            while (Markus.EstVivant)
            {
                MonstreFacile monstre = FabriqueDeMonstre();
                while (monstre.EstVivant && Markus.EstVivant)
                {
                    Markus.Attaque(monstre);
                    if (monstre.EstVivant)
                        monstre.Attaque(Markus);
                }

                if (Markus.EstVivant)
                {
                    if (monstre is MonstreDifficile)
                        cptDifficile++;
                    else
                        cptFacile++;
                }
                else
                {
                    Console.WriteLine("Snif, vous êtes mort...");
                    break;
                }
            }
            Console.WriteLine("Bravo !!! Vous avez tué {0} monstres faciles et {1} monstres difficiles. Vous avez {2} points.", cptFacile, cptDifficile, cptFacile + cptDifficile * 2);
            Console.ReadLine();
            dungeonCrawler();
        }

        private static MonstreFacile FabriqueDeMonstre()
        {

            if (random.Next(2) == 0)
            {
                return new MonstreFacile();
            }
            else
                return new MonstreDifficile();
        }


    }
}
