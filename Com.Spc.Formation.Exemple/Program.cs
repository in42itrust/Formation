﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Spc.Formation.Exemple
{
    class Program
    {
        static void Main(string[] args)
        {

            Batterie bat = new Batterie();

            bat.utilsationBaterie(50);
            Console.WriteLine(bat.Pourcentage);
            try
            {
                bat.utilsationBaterie(75);
            }
            catch( EnergieExeption e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();

        }
    }
}
