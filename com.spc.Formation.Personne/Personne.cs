﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.spc.Formation.Personne
{
    class Personne
    {

       private String nom;
       private  String prenom;

        public Personne(string nom, string prenom, int age)
        {
            Nom = nom;
            Prenom = prenom;
            Age = age;
        }

        public Personne()
        {

        }

        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }

        public int Age { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
       public string sePresenter()
        {
            if (Nom != null && Prenom != null && Age != 0)
            {
                return "je suis" + Nom + " " + Prenom;
            }
            else return " je suis amnésique";
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="complet"> permet de "forcer" la presentation complete</param>
        /// <returns></returns>
        public string  sePresenter(bool complet)
        {
              return sePresenter() + " et j'ai " + Age + " ans";
        }


    }
}
