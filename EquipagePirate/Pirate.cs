﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquipagePirate
{
    class Pirate : Personne, IPirate
    {
        public Pirate(string nom, string prenom) : base(nom, prenom)
        {
            this.Etat = true;
        }

        public string boire(string boisson)
        {
            return ("je bois du " + boisson + " sans modération");
        }

        public bool isVivant()
        {
            return Etat;
        }
    }
}
