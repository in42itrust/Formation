﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.Facricator
{
    class FabriqueDiqueDurATA : FabriqueDisqueDur
    {
        protected override DisqueDur SetControleur(string typeControleur)
        {
            DisqueDur disqueDurATA = null ;
            if ( typeControleur == "ATA")
            {
                disqueDurATA = new DisqueDurAta();

            }
            return disqueDurATA;
        }
    }
}