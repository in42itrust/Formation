﻿using System;

namespace Com.Spc.Formation.Pattern.Facricator
{
    internal class DisqueDurAta : DisqueDur
    {
        public override void Finalsier()
        {
            Console.WriteLine(" DD ata operationnel");
        }

        public override void setNbTours(string nombre)
        {
            Console.WriteLine(" nb tours : " + nombre + " tours");
        }

        public override void Tester()
        {
            Console.WriteLine(" Test en cours ...");
        }

        public DisqueDurAta()
        {
            Console.WriteLine("DD ATA cree");
        }
    }
}