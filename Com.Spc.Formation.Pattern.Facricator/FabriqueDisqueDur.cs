﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Com.Spc.Formation.Pattern.Facricator
{
    abstract class   FabriqueDisqueDur
    {

        public DisqueDur creerDisqueDur ( String controleur)
        {

            DisqueDur disqueDur;

            disqueDur = SetControleur(controleur);
            disqueDur.setNbTours("7200");
            disqueDur.Tester();
            disqueDur.Finalsier();


            return disqueDur;
        }

        protected abstract DisqueDur SetControleur(String typeControleur);
    }
}