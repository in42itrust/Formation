﻿using System;

namespace Com.Spc.Formation.Pattern.Facricator
{
    internal class DisqueDurSCSI : DisqueDur
    {
        public override void Finalsier()
        {
            Console.WriteLine(" DD SCSI operationnel");
        }

        public override void setNbTours(string nombre)
        {
            Console.WriteLine(" nb tours : " + nombre + " tours");
        }

        public override void Tester()
        {
            Console.WriteLine(" test en cours ... ");
        }

        public DisqueDurSCSI()
        {
            Console.WriteLine("DD SCSI cree");
        }
    }
}