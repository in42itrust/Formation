﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Spc.Formation.Pattern.Facricator
{
    class FabriqueDisqueDurSCI : FabriqueDisqueDur
    {
        protected override DisqueDur SetControleur(string typeControleur)
        {
            DisqueDur disqueDurSCSI = null;
            if (typeControleur == "SCSI")
            {
                disqueDurSCSI = new DisqueDurSCSI();

            }
            return disqueDurSCSI;
        }
    }
}
