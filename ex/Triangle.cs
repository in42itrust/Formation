﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex
{
    class Triangle

    {

        int[][] triangle;

       public void  triangleDePascal(int dimension)
        {
            triangle = new int[dimension][];
            for (int ligne = 1; ligne < dimension; ligne++)
            {
                // ligne courante
                triangle[ligne] = new int[ligne + 1];
                //initialisation des elements de la ligne courante ;
                triangle[ligne][1] = 1;
                for (int colone =1; colone < ligne; colone++)
                {
                    triangle[ligne][colone] = triangle[ligne - 1][colone - 1] + triangle[ligne - 1][colone];
                    triangle[ligne][ligne] = 1;
                }
            }


        }

        public void affiche()
        {
            for (int ligne =1; ligne < triangle.Length; ligne++)
            {
                // affichage de toute la ligne courante
                for (int colone = 1; colone < triangle[ligne].Length ; colone ++)
                {
                    Console.Write(triangle[ligne][colone] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }

    }
}
